# Library

## Docker app

### Run application

To run the aplication, the docker containars must be build and run as a deamon service.

Rembember that `docker` and `docker-composer` should be installed in the local machine.

To build and run the application execute: 

`$ docker-compose up -d`

### Stop application

To stop the application, get the ID's of the running docker containers:

`$ docker ps`

Then execute docker stop command:

`$ docker stop <container-id>`

Or just run this command

`$ docker stop $(docker ps -a -q)`

## Compile Assets

To compile the sass and javascript file the grunt service must be executed. In the source folder run:

`$ grunt watch`