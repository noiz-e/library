<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'Francisco Gutiérrez Cepeda',
            'email' => 'me@noizee.tech'
        ]);

        DB::table('book')->insert([
            'title' => 'El día que Nietzsche lloró',
            'author' => 'Irvin D. Yalom',
            'published_date' => '1992',
            'category_id' => 1,
            'user_id' => 1
        ]);

        #Biographical Fiction
        DB::table('category')->insert([
            'name' => 'Biografía de ficción',
            'description' => 'Novelas con personajes reales',
            'many_books' => 1
        ]);
    }
}
