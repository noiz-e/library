@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Mi libreria</h1>
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Lista</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="add-book-tab" data-toggle="tab" href="#add-book" role="tab" aria-controls="add-book" aria-selected="false">Agregar</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    </div>
    <div class="tab-pane fade" id="add-book" role="tabpanel" aria-labelledby="add-book-tab">
      <h3>Agregar libro</h3>
      {{ Form::open(array('url' => 'books', 'method' => 'post')) }}
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Titulo" name="title" />
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Autor" name="author" />
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Fecha de publicación, eg. 1999" name="author" />
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
  <script type="text/javascript">
  </script>
</div>
@endsection