<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Book;

class Library extends Controller
{
    public function index(){
        $books = Book::all();
        return view("library");
    }
}
